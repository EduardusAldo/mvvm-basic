package com.sample.mvvm.di
import com.sample.mvvm.App
import com.sample.mvvm.Helper.MySharedPreference
import com.sample.mvvm.repo.UserRepo
import com.sample.mvvm.ui.main.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by andrew on 9/16/17.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {


    // allows inject to these classes, method title is not important
    // for using @Inject
    fun inject(app : App)

    fun inject(repo : UserRepo)

    fun inject(sp : MySharedPreference)

    fun inject(activity : MainActivity)


}