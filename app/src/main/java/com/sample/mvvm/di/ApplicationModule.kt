package com.sample.mvvm.di

//import com.google.firebase.database.FirebaseDatabase
import android.content.Context
import com.google.gson.Gson
import com.sample.mvvm.App
import com.sample.mvvm.Helper.Configurations
import com.sample.mvvm.Helper.MySharedPreference
import com.sample.mvvm.repo.UserRepo
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class ApplicationModule(val app : App) {
    @Provides
    @Singleton
    fun provideContext(): Context {
        return app
    }
    @Provides
    fun provideGson(): Gson {
        val gson = Gson()
        return gson
    }
    @Provides
    fun providePicasso(): Picasso {
        return Picasso.get()
    }

    @Provides
    fun provideUserRepo(): UserRepo {
        return UserRepo()
    }
    @Provides
    fun provideRetrofit(): Retrofit{
        val retrofit = Retrofit.Builder()
            .baseUrl(Configurations.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit
    }
}
