package com.sample.mvvm.Helper

import android.app.Activity
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.*
import android.util.Log
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.fragment.app.Fragment


val TAG = Configurations.TAG +"EXTENSTION"
fun Drawable.overrideColor(@ColorInt colorInt: Int) {
    when (this) {
        is GradientDrawable -> setColor(colorInt)
        is ShapeDrawable -> paint.color = colorInt
        is ColorDrawable -> color = colorInt
        is BitmapDrawable -> {
            paint.color = colorInt
            this.setColorFilter(colorInt,PorterDuff.Mode.SRC_IN)
        }
        is VectorDrawable -> {
            val porterDuffColorFilter = PorterDuffColorFilter(colorInt, PorterDuff.Mode.SRC_ATOP)
            setColorFilter(porterDuffColorFilter)
        }
        is LayerDrawable -> setColorFilter(colorInt,PorterDuff.Mode.SRC_IN)
        else-> Log.d(TAG,"Java class:: ${this::class.java}")
    }
}


fun Activity.toast(message:String){
    Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
}
fun Fragment.toast(message:String){
    Toast.makeText(this.context,message,Toast.LENGTH_SHORT).show()
}
fun Context.toast(message:String){
    Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
}