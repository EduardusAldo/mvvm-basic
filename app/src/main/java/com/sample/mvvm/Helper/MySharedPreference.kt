package com.sample.mvvm.Helper

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.google.gson.Gson
import com.sample.mvvm.App
import java.io.Serializable
import javax.inject.Inject

class MySharedPreference(context: Context) {
    @Inject lateinit var gson : Gson
    val TAG = Configurations.TAG +"SHAREDPREF"
    val sp = context.getSharedPreferences("mysharedpref", Context.MODE_PRIVATE)
    init {
        App.graph.inject(this)
    }
    data class PhoneLocation(val phone_latitude:Float,val phone_longitude:Float,val phone_accuracy:Float):Serializable
    fun phoneLocation(latitude: Double, longitude: Double, accuracy: Double) {
        val editor = sp.edit()
        editor.putString("phone_location",gson.toJson(PhoneLocation(12F, 12F, 12F)))
        editor.apply()
    }
    fun getPhoneLocation(): PhoneLocation?{
        return gson.fromJson(sp.getString("phone_location",null), PhoneLocation::class.java)
    }

}