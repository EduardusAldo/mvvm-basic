package com.sample.mvvm.Helper

import com.sample.mvvm.data.OData

object Configurations {
    val TAG = "TESTMVVM"

    //this will change to environment setting when launching
    val BASE_URL= "https://sample.com"
}