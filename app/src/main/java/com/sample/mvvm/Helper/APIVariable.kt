package com.sample.mvvm.Helper

/**
 * Created by eduardus.aldo on 17/05/18.
 */
enum class APIVariable {
    LOADING,SUCCESS,FAILURE,SERVER_DOWN,UNAUTHORIZED,NO_CONNECTION,NULL,NO_DATA
}