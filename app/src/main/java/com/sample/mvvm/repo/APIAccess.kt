package com.sample.mvvm.repo

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by andrew on 9/18/17.
 */

interface APIAccess {

    // baca dokumentasi retrofit / okhttp
    @FormUrlEncoded
    @POST("/api/v1/sessions")
    fun login(@Field("email") name:String,@Field("password") password:String) : Call<ResponseBody>

}