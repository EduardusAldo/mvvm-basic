package com.sample.mvvm.repo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.sample.mvvm.App
import com.sample.mvvm.Helper.APIVariable
import com.sample.mvvm.Helper.Configurations
import com.sample.mvvm.data.OData
import com.sample.mvvm.data.UserData
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class UserRepo {

    @Inject lateinit var gson : Gson
    @Inject lateinit var retrofit : Retrofit
    val service by lazy { retrofit.create(APIAccess::class.java) }

    val TAG = Configurations.TAG+"USER REPO"

    init {
        Log.d(TAG,"Apollo Client")
        App.graph.inject(this)
    }
    val liveData_login = MutableLiveData<OData>()
    fun login(email:String){
        Log.d(TAG,"Request Login")
        val live_data = liveData_login
        live_data.postValue(OData(APIVariable.LOADING, ""))
        service.login("test","test").enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                Log.d(TAG,"Message: ${t?.message}")
                liveData_login.postValue(OData(APIVariable.FAILURE,t?.message?:"Error",null))
            }
            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                val json = response?.body()
                if(json!=null){
                    val resultLogin = gson.fromJson(json.string(), UserData::class.java)
                    liveData_login.postValue(OData(APIVariable.SUCCESS,resultLogin))
                }else{
                    liveData_login.postValue(OData(APIVariable.FAILURE,response?.errorBody()?.string()?:"Error",null))
                }

            }
        })
    }


}