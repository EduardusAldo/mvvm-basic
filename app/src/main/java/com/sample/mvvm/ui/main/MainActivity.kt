package com.sample.mvvm.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.sample.mvvm.App
import com.sample.mvvm.Helper.APIVariable
import com.sample.mvvm.Helper.MySharedPreference
import com.sample.mvvm.Helper.toast
import com.sample.mvvm.R
import com.sample.mvvm.repo.UserRepo
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    //by lazy is only declared when called
    val mySharedPreference by lazy { MySharedPreference(this) }
    @Inject lateinit var userRepo: UserRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        App.graph.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userRepo.liveData_login.observe(this, Observer {
            //this will observe anything change on repository
            when(it.status){
                APIVariable.SUCCESS->{

                }
                else->{
                    toast(it.statusDescription)
                }
            }
        })

        //this function call to repo
        userRepo.login("test email")

        mySharedPreference.getPhoneLocation()

    }
}
