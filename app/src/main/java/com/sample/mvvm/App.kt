@file:Suppress("DEPRECATION")

package com.sample.mvvm

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import com.sample.mvvm.Helper.Configurations
import com.sample.mvvm.di.ApplicationComponent
import com.sample.mvvm.di.ApplicationModule
import com.sample.mvvm.di.DaggerApplicationComponent

class App : Application() {


    val TAG = Configurations.TAG+"APP"
    override fun onCreate() {
        Log.d(TAG, "APP Instantiated")
        super.onCreate()
        MultiDex.install(this)


        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        _context = applicationContext
        graph =  DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
        graph.inject(this)
    }

    override fun attachBaseContext(base: Context) {
        Log.d("GLABS", " Attach Base Context")
        super.attachBaseContext(base)
    }


    companion object {
        lateinit var _context : Context
        @JvmStatic lateinit var graph: ApplicationComponent
    }
}
