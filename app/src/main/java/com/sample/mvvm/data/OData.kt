package com.sample.mvvm.data

import com.sample.mvvm.Helper.APIVariable
import java.io.Serializable

data class OData(
        val status: APIVariable,
        val statusDescription: String,
        val data: Any?
):Serializable
{
    constructor(statusCode: APIVariable,data:Any?) : this(statusCode, "", data)
    constructor(statusCode: APIVariable) : this(statusCode, "", null)
}