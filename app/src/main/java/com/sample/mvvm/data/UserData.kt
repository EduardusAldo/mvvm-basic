package com.sample.mvvm.data

import java.io.Serializable

data class UserData(
        var id:Int,
        var name: String,
        val address:String
): Serializable {}
